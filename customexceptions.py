class Error(Exception):
    # Base class for other exceptions
    pass


class BitcoinApiError(Error):
    # Raised when call to Bitcoin API fails.
    pass


class IftttApiError(Error):
    # Raised when call to IFTTT API fails.
    pass
