# Telegram Bitcoin price notifications 

Sends a Telegram message via IFTTT:
Bitcoin Price Update:<br>
Date: {{Datetime}}<br>
Bitcoin price: {{Price}}

## Version
* 1.0.0

## Dependencies

### Python
* Python 3.6
* datetime
* Requests 2.19.1

### Third Party
* IFTTT webhook
* Telegram

## Authors
* James Chadwick, 2018
