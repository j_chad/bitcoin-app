import requests
from datetime import datetime
from customexceptions import BitcoinApiError, IftttApiError


_BITCOIN_API_URL = "https://api.coinmarketcap.com/v1/ticker/bitcoin/"
_IFTTT_WEBHOOKS_URL = "https://maker.ifttt.com/trigger/{}/with/key/{}"
_IFTTT_USER_KEY = "kNz8OiVWAPYYg5XHki0AVaioD5xUGGSFFLQYpUsefRp"
_IFTTT_PRICE_EVENT = "bitcoinprice"
_output_messages = []


def _get_latest_bitcoin_price():
    response = requests.get(_BITCOIN_API_URL)
    api_result = None

    try:
        if response.status_code == 200:
            response_json = response.json()
            api_result = float(response_json[0]['price_usd'])
        else:
            raise BitcoinApiError
    except BitcoinApiError:
        api_result = response.status_code

    return api_result


def _post_ifttt_webhook(event, data):
    ifttt_event_url = _IFTTT_WEBHOOKS_URL.format(event, _IFTTT_USER_KEY)
    ifttt_response = requests.post(ifttt_event_url, json=data)

    return ifttt_response.status_code


def _bitcoin_telegram_message(event):
    global _output_messages
    data = {}
    bitcoin_response = _get_latest_bitcoin_price()

    try:
        if type(bitcoin_response) == float:
            price = "$" + str(bitcoin_response)
            date = datetime.now().strftime('%d.%m.%Y %H:%M:%S')
            _output_messages.append("Bitcoin price update: " + price + " at " + date)
            data = {'value1': price,
                    'value2': date}
            ifttt_response = _post_ifttt_webhook(event, data)
            try:
                if ifttt_response == 200:
                    _output_messages.append("Bitcoin price update posted to Telegram")
                else:
                    raise IftttApiError
            except IftttApiError:
                _output_messages.append("Error with IFTTT API, status code: " + str(ifttt_response))
        else:
            raise IftttApiError
    except IftttApiError:
        _output_messages.append("Error with Bitcoin API, status code: " + str(bitcoin_response))
    finally:
        for message in _output_messages:
            print(message)


def main():
    user_input = -1

    while user_input != 0:
        user_input = int(input("\n1 is Telegram Bitcoin price webhook\n"
                               "0 is exit\n"
                               "Choice no: "))

        if user_input == 1:
            _bitcoin_telegram_message(_IFTTT_PRICE_EVENT)
        else:
            print("No function found.")


if __name__ == '__main__':
    main()
